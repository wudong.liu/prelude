# using external formatter
# http://haskell.github.io/haskell-mode/manual/latest/Autoformating.html#Autoformating

(prelude-require-package 'eglot)

(custom-set-variables '(haskell-stylish-on-save t))

(add-to-list 'eglot-server-programs '(haskell-mode . ("stack" "exec" "ghcide" "--" "--lsp")))

(add-hook 'haskell-mode-hook 'eglot-ensure)
